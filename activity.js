db.users.insertMany([
			{
				firstName: "Jane",
				lastName: "Doe",
				age: 21,
				contact: {
				    phone: "87654321",
				    email: "janedoe@gmail.com"
				},
				courses: [ "CSS", "Javascript", "Python" ],
				department: "HR"
			},
		    {
		        firstName: "Stephen",
		        lastName: "Hawking",
		        age: 50,
		        contact: {
		            phone: "87654321",
		            email: "stephenhawking@gmail.com"
		        },
		        courses: [ "Python", "React", "PHP" ],
		        department: "HR"https://chat.google.com/u/0/api/get_attachment_url?url_type=FIFE_URL&content_type=image%2Fjpeg&attachment_token=AIGoCJla8l04SwEkkLgGqy1b7widA66ZwzfqmzY%2FBYDVMQgRDdegPC9fv4P2UFW6qCPEbXXvo3cvQ08s0LJmI7Q9kIJKazCw70vq2U0iSpa3uhD%2FgzE%2Fr61rvTg5JVS14OKUrp6msa73sY2CaxbpLa5L6QJRoBzl9QYgG45TdcqDiYPsTUbsE1ctzBP%2FqrU%2FqE6o3tK9SyBsXsglW3shQUBgIUpg7raEQzJIwx6%2BJrv3LprutrdnWWVnVt8RQs7LEMcG27AHQBfAr031L1s5hCY7iVv5v%2FwDbpQd79rLMSOL%2BuDd09SmYgaU%2BlD4Rkh1Q2ZPUMy9DI2F3zwK25EGwZlHeNXSvLHp%2BXUlZPLHtjMIvc6rR1icOrJ4E0RZmHVVnG7%2FK%2F%2BQL2t2aANDuMrZ6IJ1FmEwcOqPxO3qdGiaxASyHyiFAyxNzq602ICNQxCOYAn6guLT2OgVQfDtt9cjRxJpMYLt%2BiucvkLtCv3Mg%2FXhZ2rkmG%2F%2FltD1vg%3D%3D&sz=w1268-h998
		    },
		    {
		        firstName: "Neil",
		        lastName: "Armstrong",
		        age: 82,
		        contact: {
		            phone: "87654321",
		            email: "neilarmstrong@gmail.com"
		        },
		        courses: [ "React", "Laravel", "Sass" ],
		        department: "HR"
		    }
		]);



// 1. Create an activity.js file on where to write and save the solution for the activity.
// 2. Find users with letter s in their first name or d in their last name.



/*db.users.find({ $or: [{firstName: {$regex: "s", $options: "$i"}}, {lastName: {$regex: "d", $options: "$i"}}]});

db.users.find({ $or: [{firstName: {$regex: "s", $options: "$i"}}, {lastName: {$regex: "d", $options: "$i"}]},

{
firstName: 1,
lastName: 1,
 _id: 0 
}

 });*/

db.users.find( { $or: [ 
    { firstName: 
        { $regex: 's', $options: '$i'}}, { lastName: {$regex: 'd', $options: '$i'}} ] }, 
    {
    firstName: 1,
    lastName: 1,
    _id: 0
    }
);

// - Use the $or operator.
// - Show only the firstName and lastName fields and hide the _id field.
// 3. Find users who are from the HR department and their age is greater than or equal to 70.

db.users.find({$or: [{department: "HR"}, {age: {$gte: 70}}]});

// - Use the $and operator
// 4. Find users with the letter e in their first name and has an age of less than or equal to 30.
// - Use the $and, $regex and $lte operators

db.users.find({$and: [ { firstName: {$regex: "e", $options: "$i"}}, {age: { $lte: 30 }}]});

// 5. Create a git repository named S29.
// 6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 7. Add the link in Boodle.
